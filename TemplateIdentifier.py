
import os
import math
from PyPDF2 import PdfFileReader as pfr
from wand.image import Image
from wand.color import Color
import json
import shutil
##Added 12/02/2018
import datetime
import numpy as np

import cv2
import pytesseract
import pandas as pd
from PIL import Image as Image2
import nltk
from nltk import sent_tokenize


global js, ocr_txt
global clientEmailAddress
js = open("C:/Users/ravi.kumar/Desktop/Robotics/Others/Listener/fwdocrdatajsonmodel/OCRProcessedDataJSON - Copy.txt").read()

ocr_txt = json.loads(js)
global outputFinal
outputFinal = {}
pointer=0



##Added on 12/02/2018

ErrorMsg = [{"Errorcode":"500",
             "Description":"",
             "process_type":"OCR_PROCESS",
             "createdate":"",
             "Exception":"",
             "ProcessBy":"",
             "processId":"",
             "uniqueidmessage":""}]

##*********************** Listener *****************************

import stomp
import time
import sys
import os

text = None

class SampleListener(object):
    def on_message(self, headers, msg):
        global text
        text = msg
user = os.getenv("ACTIVEMQ_USER") or "Admin"
password = os.getenv("ACTIVEMQ_PASSWORD") or "Admin"
host = os.getenv("ACTIVEMQ_HOST") or "localhost" ##"104.198.143.255"
port = os.getenv("ACTIVEMQ_Port") or 61616 ##61613
destination = sys.argv[1:2] or ["DLQ"] ##["/queue/EDB.INBOUNDOCRPROCESS"]
destination = destination[0]
conn = stomp.Connection(host_and_ports = [(host,port)])
conn.set_listener('SampleListener', SampleListener())
conn.start()
conn.connect(login=user, passcode = password)
conn.subscribe(destination=destination, id=1, ack='auto')
time.sleep(5)
conn.disconnect()

##**************************** Listener END ****************************


##**************************** Sender START ****************************


def Sender_to_Queue(outputFinal):
    import time
    import sys
    import os
    import stomp


    user = os.getenv("ACTIVEMQ_USER") or "Admin"
    password = os.getenv("ACTIVEMQ_PASSWORD") or "Admin"
    host = os.getenv("ACTIVEMQ_HOST") or "localhost" ##"104.198.143.255"
    port = os.getenv("ACTIVEMQ_Port") or 61616  ##61613
    destination = sys.argv[1:2] or ["DLQ"]     ##["/queue/event"]
    destination = destination[0]
    messages = 1
    conn = stomp.Connection(host_and_ports = [(host,port)])
    conn.start()
    conn.connect(login=user, passcode = password)
    print("*************Sending OutputFinal ***********************")
    final_data = []
    for i in outputFinal:
        final_data.append(outputFinal[i])
    print(final_data)
    for i in range(0,messages):
        conn.send(json.dumps(final_data),destination = destination, persistent = 'false')
    ##    conn.send("SHUTDOWN",destination = destination, persistent = 'false')
    conn.disconnect()


##******************************* Sender End *****************************************

##**************************** Error Queue START ****************************

##Added on 12/02/2018

def Error_Queue(ErrorMsg):
    import time
    import sys
    import os
    import stomp


    user = os.getenv("ACTIVEMQ_USER") or "Admin"
    password = os.getenv("ACTIVEMQ_PASSWORD") or "Admin"
    host = os.getenv("ACTIVEMQ_HOST") or "localhost" ##"104.198.143.255"
    port = os.getenv("ACTIVEMQ_Port") or 61616  ##61613
    destination = sys.argv[1:2] or ["DLQ"]     ##["/queue/event"]
    destination = destination[0]
    messages = 1
    conn = stomp.Connection(host_and_ports = [(host,port)])
    conn.start()
    conn.connect(login=user, passcode = password)
    print("*************Sending OutputFinal ***********************")
    final_data = ErrorMsg
    print(final_data)
    print("Error msg sendnig to Queue")
        
    for i in range(0,messages):
        conn.send(json.dumps(final_data),destination = destination, persistent = 'false')
    ##    conn.send("SHUTDOWN",destination = destination, persistent = 'false')
    conn.disconnect()


##******************************* Error Queue End *****************************************



##******************************* PDF to IMAGE *****************************************

def create_img(self):
    ##Added on 12/04/2018

    ipfile = fileLocation
    print("Execution Started")
    print("***************************************************************** b")
    print("pdf to image conversion in progress...")
    print(" ")    
#Read file and count pages
    try:
        global reader
        reader = pfr(open(ipfile,'rb')) 
    except FileNotFoundError:
        D_time = str(datetime.datetime.now())
        ErrorMsg = [{"Errorcode":"100",
             "Description":"Please check might be file or folder location is incorrect",
             "process_type":"OCR_PROCESS",
             "createdate":D_time,
             "Exception":"",
             "ProcessBy":"",
             "processId":"",
             "uniqueidmessage":"",
             "inputmsg":text}]
        Error_Queue(ErrorMsg)
        print("Msg Sent to Queue")
        sys.exit()

    page_count = reader.getNumPages()

    global img_folder
    img_folder = '1.Test/Test/'

    images = []

    with Image(filename= ipfile, resolution=300) as img:
            img.compression_quality = 100
            img.background_color = Color('white')
            img.alpha_channel='remove'
##            img.save(filename=img_folder+'img.jpg')
            img.save(filename=img_folder+fileName+'.jpg')
        
    print("*****************************************************************")
    print("PDF converted to image:")
    print(" ")
    print("*****************************************************************")
    print("Please wait while converting selection to text:")
    print(" ")
    print("*****************************************************************")
    pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'
##Selection from image with given coordinates
    images = []
    img_folder = img_folder
    for img_file in os.listdir(img_folder):
            img_file = os.path.join(img_folder,img_file)
            images.append(img_file)
    print("*****************************************************************")

##***************************************************************************
##***************************************************************************

def check_for_one_page(img_folder,page_no):

    js = open("C:/Users/ravi.kumar/Desktop/Robotics/Others/Listener/fwdocrdatajsonmodel/OCRProcessedDataJSON.txt").read()

    ocr_txt = {}
    ocr_txt = json.loads(js)

    cord = ["fieldZoneMinX","fieldZoneMinY","fieldZoneMaxX","fieldZoneMaxY","textExtracted"]
    keyword_received = list(final.keys())

    final_keyword = []
    final_coordinates = []

    a = 0

    for i in keyword_received:
        a += 1
        
        provided_keyword = list(final.keys())[a-1]

        pn = page_no[provided_keyword]

        if pn <2:
            img_location_with_page = img_folder+fileName+'.jpg'
            image = cv2.imread(img_location_with_page)
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            img_location_with_page = img_folder+fileName+'-'+str(pn-1)+'.jpg'
            image = cv2.imread(img_location_with_page)
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        lowercase_keyword = provided_keyword.lower()
        lowercase_keyword = lowercase_keyword.replace('\n',' ')
        lowercase_keyword = lowercase_keyword.replace('~', ' ')
        lowercase_keyword = lowercase_keyword.replace(':',' ')

        count_of_keyword = len(lowercase_keyword.split())
        final_coordiantes= final.get(i)

        x1 = int(final_coordiantes[0])
        y1 = int(final_coordiantes[1])
        x2 = int(final_coordiantes[2])
        y2 = int(final_coordiantes[3])
        width = int(final_coordiantes[4])   ## Added on 12/02
        height = int(final_coordiantes[5])  ## Added on 12/02
        print(lowercase_keyword,x1,y1,x2,y2,width, height) ## Added on 12/02

        crop_img = gray_image[y1:y1+height,x1:x1+width]
## Added on 12/09/2018
        kernal = np.ones((1,1),np.uint8)
        crop_img = cv2.dilate(crop_img, kernal, iterations = 1)
        crop_img = cv2.erode(crop_img,kernal, iterations = 1)
##        gray_image = cv2.imwrite(img_folder + "thres.png", gray_image)
        text = pytesseract.image_to_string(crop_img)
        text = text.replace('\n',' ')
        varTemp=""
        
#Check if character is available into the selected co-ordinates. If "yes" print else check in image

        for m in txt["allTemplateFields"]:
            print(m,templateId)
            if int(m) == templateId: #allTemplateFieldIndicator ##k:
                temp = txt["allTemplateFields"][m]
                for z in temp:
                    if z["fieldName"] == provided_keyword:

                        pointer=allTemplateFieldIndicator

                        ocr_txt["extractOcrDataJson"].append({provided_keyword:text.replace(lowercase_keyword,'')})
                        ocr_txt["inputTemplateTemplateName"] = z["inputTemplateTemplateName"]                                  
                        ocr_txt["inputTemplateId"] = z["inputTemplateId"]
                        ocr_txt["clientEmailAddress"] = clientEmailAddress
                        ocr_txt["emailMessageId"] = messageId
                        ocr_txt["searchId"].append(z["fieldName"])
##                        ocr_txt["searchId"] = z["fieldName"]
                        ocr_txt["attachmentfileName"] = fileName
                        print(ocr_txt)


            else:
                if int(m) == allTemplateFieldIndicator: ##k:
                    ocr_txt["extractOcrDataJson"].append({provided_keyword:'fieldName not found'})

        varTemp = allTemplateFieldIndicator ##str(k) ##+ str(foo)
        finalDict(ocr_txt,varTemp)

def finalDict(text,key):
    outputFinal.update({key:text})

    ocr_text={}
    
    
## *******************END Json file output*********************


##***************************************************************************
##                      Move files to a different folder
##***************************************************************************

def move_files(path,moveto):
    files = os.listdir(path)
    files.sort()
    for f in files:
        src = path + f
        dst = moveto + f
        shutil.move(src,dst)

##***************************************************************************
##***************************************************************************


##************** Extract Values from Listener *****************************

coordinates = ["fieldZoneMinX","fieldZoneMinY","fieldZoneMaxX","fieldZoneMaxY","width","height"]

try:
    global txt
    txt = json.loads(text)
except TypeError:
    D_time = str(datetime.datetime.now())
    ErrorMsg = [{"Errorcode":"101",
             "Description":"Found TypeError: Please check might be Queue is empty!",
             "process_type":"OCR_PROCESS",
             "createdate":D_time,
             "Exception":"",
             "ProcessBy":"",
             "processId":"",
             "uniqueidmessage":"",
             "inputmsg":text}]
    Error_Queue(ErrorMsg)
    print("Msg Sent to Queue")
    sys.exit()

##********** FileLocation and if template is matching *******************

check = txt["emailAttachmentDataList"]

j = 0
for i in check:
    j += 1
    c = check[j-1]
    attachmentData = c["attachmentData"]
    varCheck = c["isTemplateMatch"]

## Added on 12/09/2018
    
    if varCheck == True:
        print("It is true part")
        templateId = c["templateId"]
        
        for data in attachmentData:
            if data == "fileName":
                fileName = attachmentData["fileName"]
            elif data == "fileLocation":
                fileLocation = attachmentData["fileLocation"]
            elif data == "fileExtension":
                fileExtension = attachmentData["fileExtension"]
            elif data == "id":
                allTemplateFieldIndicator = attachmentData["id"]
            elif data == "messageId":
                messageId = attachmentData["messageId"]
            elif data == "clientEmailAddress":
                clientEmailAddress = attachmentData["clientEmailAddress"]

        if fileName[-4:].lower() == '.pdf':
            fileLocation = attachmentData["fileLocation"] + attachmentData["fileName"]
        else:
    ##Added on 12/04/2018
            D_time = str(datetime.datetime.now())
            ErrorMsg = [{"Errorcode":"102",
                     "Description":"PDFError: Might be received file is other than pdf file",
                     "process_type":"OCR_PROCESS",
                     "createdate":D_time,
                     "Exception":"",
                     "ProcessBy":"",
                     "processId":"",
                     "uniqueidmessage":"",
                     "inputmsg":text}]
            Error_Queue(ErrorMsg)
            print("Msg Sent to Queue")
            sys.exit()
            
        print(fileName,fileLocation,fileExtension,allTemplateFieldIndicator,messageId,clientEmailAddress)
        txt2 = txt["allTemplateFields"]
        for k in txt2:
            if int(k) == templateId:

                txt3 = txt2[k]
                global final, page_no
                final = {}
                page_no = {}
                if varCheck == True:
                    for n in txt3:
                        final[n["fieldName"]] = [n[c] for c in coordinates]
                        page_no[n["fieldName"]] = n["pageNumebr"]

                create_img(fileLocation)
                check_for_one_page(img_folder,page_no)
                path = "C:/Users/ravi.kumar/Desktop/Robotics/Others/1.Test/Test/"
                moveto = "C:/Users/ravi.kumar/Desktop/Robotics/Others/1.Test/Archieve/"
                move_files(path,moveto)
                print("True part ends")
##    print("********************Sending result to ActiveMQ to send out***************************")

    else:

        print("It is false part")
        for data in attachmentData:
            if data == "fileName":
                fileName = attachmentData["fileName"]
            elif data == "fileLocation":
                fileLocation = attachmentData["fileLocation"]
            elif data == "fileExtension":
                fileExtension = attachmentData["fileExtension"]
            elif data == "id":
                allTemplateFieldIndicator = attachmentData["id"]
            elif data == "messageId":
                messageId = attachmentData["messageId"]
            elif data == "clientEmailAddress":
                clientEmailAddress = attachmentData["clientEmailAddress"]

        if fileName[-4:].lower() == '.pdf':
            fileLocation = attachmentData["fileLocation"] + attachmentData["fileName"]
        else:
            D_time = str(datetime.datetime.now())
            ErrorMsg = [{"Errorcode":"102",
                     "Description":"PDFError: Might be received file is other than pdf file",
                     "process_type":"OCR_PROCESS",
                     "createdate":D_time,
                     "Exception":"",
                     "ProcessBy":"",
                     "processId":"",
                     "uniqueidmessage":"",
                     "inputmsg":text}]
            Error_Queue(ErrorMsg)
            print("Msg Sent to Queue")
            sys.exit()
            
        create_img(fileLocation)

        img_location_with_page = img_folder+fileName+'.jpg'
        image = cv2.imread(img_location_with_page)
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        kernal = np.ones((1,1),np.uint8)
        gray_image = cv2.dilate(gray_image, kernal, iterations = 1)
        gray_image = cv2.erode(gray_image,kernal, iterations = 1)
        Temp_text = pytesseract.image_to_string(gray_image)
        Temp_text = text.replace('\n',' ')
        sentance = nltk.sent_tokenize(Temp_text)

        allTemplate = txt["allTemplate"]
        for T_id in allTemplate:
            for tidentifier in allTemplate[T_id]:
                if tidentifier == "templateIdentifier":
                    templateIdentifier = allTemplate[T_id]["templateIdentifier"]
                elif tidentifier == "templateType":
                    templateType = allTemplate[T_id]["templateType"]
                elif tidentifier == "templateName":
                    templateName = allTemplate[T_id]["templateName"]
                elif tidentifier == "templateDescription":
                    templateDescription = allTemplate[T_id]["templateDescription"]
                
            for line in sentance:
                for word in nltk.word_tokenize(line):
                    if word == templateIdentifier:
                        txt2 = txt["allTemplateFields"]
                        for s in txt2:
                            if int(s) == int(T_id):
                                templateId = int(T_id)
                                txt3 = txt2[s]
##                                global final, page_no
                                final = {}
                                page_no = {}
                                for n in txt3:
                                    final[n["fieldName"]] = [n[c] for c in coordinates]
                                    page_no[n["fieldName"]] = n["pageNumebr"]

                                create_img(fileLocation)
                                check_for_one_page(img_folder,page_no)
                                path = "C:/Users/ravi.kumar/Desktop/Robotics/Others/1.Test/Test/"
                                moveto = "C:/Users/ravi.kumar/Desktop/Robotics/Others/1.Test/Archieve/"
                                move_files(path,moveto)
                                print("False part ends")
                        
Sender_to_Queue(outputFinal)

