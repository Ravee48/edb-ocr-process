
import os
import math
from PyPDF2 import PdfFileReader as pfr
from wand.image import Image

import cv2
import pytesseract
import pandas as pd
from PIL import Image as Image2
import nltk
from nltk import sent_tokenize


ipfile = input('Enter FileName: ')
print("Execution Started")
print("***************************************************************** b")

print("pdf to image conversion in progress...")
print(" ")


#Read file and count pages
reader = pfr(open(ipfile,'rb')) 
page_count = reader.getNumPages()

##opfile = ipfile[:-4] + '.jpg'
img_folder = '1.Test/Test/'
##file_path = img_folder + opfile

##Convert PDF/Scanned file into Image (.jpg)

images = []

with Image(filename= ipfile, resolution=300) as img:
        img.compression_quality = 100
        img.save(filename=img_folder+'img.jpg')

print("*****************************************************************")

print("PDF converted to image:")
print(" ")
print("*****************************************************************")

print("Please wait while converting selection to text:")
print(" ")
print("*****************************************************************")


pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'

##Selection from image with given coordinates

images = []
img_folder = img_folder
for img_file in os.listdir(img_folder):
	img_file = os.path.join(img_folder,img_file)
	images.append(img_file)

print("*****************************************************************")


## Check based on the coordinates and Keyword provided by front end person

provided_keyword = 'ship to'
lowercase_keyword = provided_keyword.lower()
lowercase_keyword = lowercase_keyword.replace('\n',' ')
lowercase_keyword = lowercase_keyword.replace('~', ' ')
lowercase_keyword = lowercase_keyword.replace(':',' ')
count_of_keyword = len(lowercase_keyword.split())

x1 = 650
y1 = 1100
x2 = 100
y2 = 1100

Area_Of_Rectangle = (y1-x1)*(y2-x2)


##***************************************************************************
## Define function for pdf that has only 1 page in it
##***************************************************************************

def check_for_one_page(self):
    image = cv2.imread(img_location_with_page)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    crop_img = gray_image[x1:y1,x2:y2]
    cv2.imshow("cropped", crop_img)

## Calculating Distance using Distance Formula

##distance = math.hypot((100-650),(1100-1100))
#for variables
##distance = math.ceil(math.hypot((x2-x1),(y2-y1)))

#For Quadratic equation (AX^2+BX+C)calculate C in advance

##C = distance**2-(y2-y1)**2-x1**2

## Converting selection of Image into text

##text = pytesseract.image_to_string(Image.open(crop_img))
    text = pytesseract.image_to_string(crop_img)
##    print(text)

#Check if character is available into the selected co-ordinates. If "yes" print else check in image
    sent = nltk.sent_tokenize(text)
    for word in sent:
        ##if "issuing branch"  in word.lower():
        if "ship to"  in word.lower():
            print(text)
    else:
        print('Selected word not found in the selected pdf or not able to convert the pdf properly')


##***************************************************************************      
## Define function for pdf more than 1 page in pdf
##***************************************************************************



def check_for_otherthan_onepage(self):
    x1 = 650
    y1 = 1100
    height = y1-x1
    x2 = 100
    y2 = 1100
    width = y2-x2

    Area_Of_Rectangle = (y1-x1)*(y2-x2)

######***********************CHANGED BECAUSE NEW PROCESS IMPLIMENTED THAT IS AREA OF RECTANGLE**************************
#### Calculating Distance using Distance Formula
##
####distance = math.hypot((100-650),(1100-1100))
#####for variables
##    distance = math.ceil(math.hypot((x2-x1),(y2-y1)))
##
#####For Quadratic equation (AX^2+BX+C)calculate C in advance
##    C = (distance**2-(y2-y1)**2-x1**2)*(-1)
####****************************************************************************************************************

### Load the image
##img = cv2.imread('1.Test/Test/img-2.jpg')
    mul_img = cv2.imread(img_location_with_page)

### convert to grayscale
    gray = cv2.cvtColor(mul_img,cv2.COLOR_BGR2GRAY)

# smooth the image to avoid noises
    gray = cv2.medianBlur(gray,5)

# Apply adaptive threshold
    thresh = cv2.adaptiveThreshold(gray,255,1,1,11,2)
##    thresh_color = cv2.cvtColor(thresh,cv2.COLOR_GRAY2BGR)

# Apply some dilation and erosion to join the gaps - change iteration to detect more or less area's
    thresh = cv2.dilate(thresh,None,iterations = 15)
    thresh = cv2.erode(thresh,None,iterations = 15)

# Find the contours
    image,contours,hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

#Calculate value of X2 using functions so that it can be used for new coordinates

    def x2_coordinates(x1,y1,y2,Area_Of_Rectangle):
        if Area_Of_Rectangle > 0:
                x2 = y2 - (Area_Of_Rectangle/(y1-x1))
        else:
                x2 = -(y2 - (Area_Of_Rectangle/(y1-x1)))
        return(x2)

# For each contour, find the bounding rectangle and draw it

##height = 450
##width = 900
##X = 100

    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)

        if x >= 10:
                a = x-10
        else:
                a = 0
##        a = x-10

        c = w+15

        if y >= 10:
                b = y-10
        else:
                b = 0
##        b = y-10

        d = h+15

        cv2.rectangle(mul_img,(a,b),(a+c,b+d),(0,255,0),2)
        crop_test_img = mul_img[b:b+d,a:a+c]
##        cv2.imshow('Image',crop_test_img)     # what all emage get converted
##        cv2.waitKey(1000)
        text = pytesseract.image_to_string(crop_test_img)
        text = text.replace('\n',' ')
        text = text.replace('~', ' ')
        text = text.replace(':',' ')
    ##    print(text)
        sent = nltk.sent_tokenize(text)
        for word in sent:
                match = ' '.join(word.split()[:count_of_keyword])
##                print('****************************************')
##                print(word)
##                print('****************************************')
                if lowercase_keyword in match.lower():
                        print(word)
                        x1 = b
                        y1 = y1
                        y2 = y2
                        x2 = abs(x2_coordinates(x1,y1,y2,Area_Of_Rectangle))  #calculation for x2
                        
##                        for value in z:
##                            if z[0] > z[1]:
##                                x = abs(math.ceil(z[0]))
##                            else:
##                                x = abs(math.ceil(z[1]))
                        
                        crop_final_img = mul_img[x1:x1+height,x:x+width]
    ##                    crop_final_img = img[b:b+height,X:X+width]
                        print("extracted coordinates: ",b,b+d,a,a+c)
                        print("************************************")
                        print("modified coordinates: ",x1,x1+height,x2,x2+width)
                        print(pytesseract.image_to_string(crop_final_img))
##                        cv2.destroyAllWindows()


##***************************************************************************
##                      END Def
##***************************************************************************


##***************************************************************************
##      Start loop to fetch the data from pdf per the given coordinates
##***************************************************************************


## Check if page count is one then go ahead for single page else check for the other pdf pages available in the pdf
if page_count < 2:
    page_no = ''
    img_location_with_page = img_folder+'img'+'.jpg'
    check_for_one_page(img_location_with_page)
else:
    page_no = page_count
    for pn in range(0,page_no):
    #write a loop to check if it is working or not
    ##    page_no = input('Please enter page no: ')
        img_location_with_page = img_folder+'img-'+str(pn)+'.jpg'
        print(img_location_with_page)
        check_for_otherthan_onepage(img_location_with_page)
##        if x != 0:
##            break
    
