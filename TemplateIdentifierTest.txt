{

	"allTemplateFields": {

		"7": [

			{

				"id": 9,

				"fieldName": "supplier",

				"fieldZoneMinX": 297.0,

				"fieldZoneMinY": 794.0,

				"fieldZoneMaxX": 886.0,

				"fieldZoneMaxY": 977.0,

				"width": 588.0,

				"height": 182.0,

				"sequence": 1,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 7,

				"inputTemplateTemplateName": "pawan-test1"

			},

			{

				"id": 10,

				"fieldName": "issued by",

				"fieldZoneMinX": 1498.0,

				"fieldZoneMinY": 794.0,

				"fieldZoneMaxX": 2332.0,

				"fieldZoneMaxY": 1054.0,

				"width": 833.0,

				"height": 259.0,

				"sequence": 2,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 7,

				"inputTemplateTemplateName": "pawan-test1"

			},

			{

				"id": 11,

				"fieldName": "responsible",

				"fieldZoneMinX": 1687.0,

				"fieldZoneMinY": 1173.0,

				"fieldZoneMaxX": 2020.0,

				"fieldZoneMaxY": 1236.0,

				"width": 332.0,

				"height": 63.0,

				"sequence": 3,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 7,

				"inputTemplateTemplateName": "pawan-test1"

			},

			{

				"id": 12,

				"fieldName": "email",

				"fieldZoneMinX": 1694.0,

				"fieldZoneMinY": 1313.0,

				"fieldZoneMaxX": 2153.0,

				"fieldZoneMaxY": 1376.0,

				"width": 458.0,

				"height": 63.0,

				"sequence": 4,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 7,

				"inputTemplateTemplateName": "pawan-test1"

			},

			{

				"id": 13,

				"fieldName": "suppl_email",

				"fieldZoneMinX": 434.0,

				"fieldZoneMinY": 1050.0,

				"fieldZoneMaxX": 836.0,

				"fieldZoneMaxY": 1113.0,

				"width": 402.0,

				"height": 63.0,

				"sequence": 5,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 7,

				"inputTemplateTemplateName": "pawan-test1"

			}

		],

		"8": [

			{

				"id": 14,

				"fieldName": "supplier",

				"fieldZoneMinX": 297.0,

				"fieldZoneMinY": 794.0,

				"fieldZoneMaxX": 886.0,

				"fieldZoneMaxY": 977.0,

				"width": 588.0,

				"height": 182.0,

				"sequence": 1,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 8,

				"inputTemplateTemplateName": "pawan-test2"

			},

			{

				"id": 15,

				"fieldName": "issued by",

				"fieldZoneMinX": 1498.0,

				"fieldZoneMinY": 794.0,

				"fieldZoneMaxX": 2332.0,

				"fieldZoneMaxY": 1054.0,

				"width": 833.0,

				"height": 259.0,

				"sequence": 2,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 8,

				

				"inputTemplateTemplateName": "pawan-test2"

			},

			{

				"id": 16,

				"fieldName": "responsible",

				"fieldZoneMinX": 1687.0,

				"fieldZoneMinY": 1173.0,

				"fieldZoneMaxX": 2020.0,

				"fieldZoneMaxY": 1236.0,

				"width": 332.0,

				"height": 63.0,

				"sequence": 3,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 8,

				"inputTemplateTemplateName": "pawan-test2"

			},

			{

				"id": 17,

				"fieldName": "email",

				"fieldZoneMinX": 1694.0,

				"fieldZoneMinY": 1313.0,

				"fieldZoneMaxX": 2153.0,

				"fieldZoneMaxY": 1376.0,

				"width": 458.0,

				"height": 63.0,

				"sequence": 4,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 8,

				"inputTemplateTemplateName": "pawan-test2"

			},

			{

				"id": 18,

				"fieldName": "suppl_email",

				"fieldZoneMinX": 434.0,

				"fieldZoneMinY": 1050.0,

				"fieldZoneMaxX": 836.0,

				"fieldZoneMaxY": 1113.0,

				"width": 402.0,

				"height": 63.0,

				"sequence": 5,

				"isTemplateIdentifier": null,

				"isLabel": null,

				"pageNumebr": 1,

				"fieldValidationRequire": null,

				"fieldValidationRule": "",

				"inputTemplateId": 8,

				"inputTemplateTemplateName": "pawan-test2"

			}

		]

	},

	"emailMsg": {

		"id": 49,

		"messageId": "<CACRUEfnNKMgQB5_2SqSaSCDyKNR2+hEJV+0+n50bp_+OW3HCFQ@mail.gmail.com>",

		"emailSubject": "Fwd: pawan-test-xxx",

		"emailBody": null,

		"status": null,

		"clientEmailAddress": "pawan.garg@edatablock.com",

		"receiveFrom": "Pawan Garg <pawan.garg@edatablock.com>",

		"receivedTime": {

			"nano": 403000000,

			"epochSecond": 1543513083

		},

		"numberOfAttachments": 2,

		"attachments": "PAWAWB12345.PDF,PAWINV12345.PDF,",

		"clientId": 1,

		"clientClientEmailAddress": null

	},

	"emailAttachmentDataList": [

		{


			"attachmentData": {

				"id": 38,

				"messageId": "<CACRUEfnNKMgQB5_2SqSaSCDyKNR2+hEJV+0+n50bp_+OW3HCFQ@mail.gmail.com>",

				"clientEmailAddress": "Pawan Garg <pawan.garg@edatablock.com>",

				"fileName": "PAWAWB12345.PDF",

				"fileExtension": ".PD",

				"fileLocation": "C:/Users/ravi.kumar/Desktop/Robotics/Others/Listener/fwdpawantestxxx/",

				"documentType": null,

				"emailMessagesId": 49,

				"emailMessagesMessageId": null

			},

			"isTemplateMatch": false

		},

		{

			"templateId": 7,

			"attachmentData": {

				"id": 39,

				"messageId": "<CACRUEfnNKMgQB5_2SqSaSCDyKNR2+hEJV+0+n50bp_+OW3HCFQ@mail.gmail.com>",

				"clientEmailAddress": "Pawan Garg <pawan.garg@edatablock.com>",

				"fileName": "PAWINV12345.PDF",

				"fileExtension": ".PD",

				"fileLocation": "C:/Users/ravi.kumar/Desktop/Robotics/Others/Listener/fwdpawantestxxx/",

				"documentType": null,

				"emailMessagesId": 49,

				"emailMessagesMessageId": null

			},

			"isTemplateMatch": true

		}

	],

	"allTemplate": {

		"7": {

			"id": 7,

			"templateName": "pawan-test1",

			"templateDescription": "testing",

			"templateType": "INV",

			"isStandardTemplate": true,

			"isActive": null,

			"createDate": null,

			"createdBy": "",

			"updateDate": null,

			"templateIdentifier": "",

			"numberOfPages": 1,

			"updatedBy": "",

			"clientId": 1,

			"clientClientName": "Airfrance"

		},

		"8": {

			"id": 8,

			"templateName": "pawan-test2",

			"templateDescription": "testing",

			"templateType": "AWB",

			"isStandardTemplate": true,

			"isActive": null,

			"createDate": null,

			"createdBy": "",

			"updateDate": null,

			"templateIdentifier": "SUPPLIER",

			"numberOfPages": 1,

			"updatedBy": "",

			"clientId": 1,

			"clientClientName": "Airfrance"

		}

	}

}